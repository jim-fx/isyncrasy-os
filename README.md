# About ISyncrasy-OS

**merge requests are _very_ welcome**

ISyncrasyOS is a very simple simulated OS running on svelte in your browser.

## Features

- [FileSystem](#filesystem)
- [Terminal](#terminal)
- [GUI Program](#gui-programs)
- [Roadmap](#roadmap)

## FileSystem

The filesystem is a very simple json object.

It was persisted to localStorage in the beginning but at the moment that is disabled due to issues with updates.

## Terminal

Its a very simple terminal. At the moment you can start executables from the filesystem and you have a limited history.

## GUI Programs

ISyncrasyOS has a very simple windows manager, which allows svelte components to run inside a window, and persist window state between reruns.

## Roadmap

_close_

- Autocomplete for the terminal based in the history
- Cleanup of the terminal code
- Persisted filesystem with handling of application updates

_far_

- multi user
