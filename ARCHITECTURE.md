# Architecture

## Components

The ISyncrasyOS can roughly be seperated into three modules.

### Stores

Here lives the state of the application. The state can also be roughly seperated into three stores.

// _In a future version i think it would be good to save all the other stores in the filesystem_

<img src="assets/docs/stores.svg" alt="state stores" width="100%">

### Programs

All the executables live under src/programs.

> On an actual os the executables are actual files that live in the filesystem. Because we save the filesystem as json, and json can't _(we don't want to use eval)_ contain executable code we have to save them somewhere seperate.

**Execution**

There are two main ways to start a program, either the user clicks on an icon, or the user executes the program in the terminal.

<img src="assets/docs/execution.svg" alt="execution path" width="100%">

**Program Structure**

_A simple cli program:_

```javascript
export default {
  id: "echo",
  exec: function ({ command, rest }, stdout) {
    // stdout takes either a string, or array of strings
    // and prints them to the terminal

    if (command !== "echo") return false;
    return stdout(rest);
  },
};
```

_A simple gui program:_

```javascript
export default {
  id: "echo",
  app: SvelteComponent
  props:{
    option: false
  },
  exec: function (options) {

    /*
      [OPTIONAL]
      when the program is called from the terminal
      options are the parsed command line input
    */

    if(options){
      this.props = {
        test: options.flags
      }
    }

    window.create(this);
  },
};
```

### Screens

The state contains a key called "screen". According to this we can show different UI's. At the moment we have a "boot" ui and a "desktop" ui. Furthermore we could implement a "login" ui.
