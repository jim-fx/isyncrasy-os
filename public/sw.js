
const version = "0.6.18";
const cacheName = `isyncrasy-${version}`;
self.addEventListener('install', e => {
  e.waitUntil(
    caches.open(cacheName).then(cache => {
      return cache.addAll([
        `/`,
        `/index.html`,
        `/global.css`,
        `/build/bundle.css`,
        `/build/bundle.js`,
      ])
        .then(() => self.skipWaiting());
    })
  );
});

self.addEventListener('activate', event => {
  event.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', event => {

  event.respondWith(
    fetch(event.request)
      .then(response => caches.open(cacheName).then(cache => {
        cache.put(event.request.url, response.clone());
        return response;
      }))
      .catch(() => caches.match(event.request))

  );
});