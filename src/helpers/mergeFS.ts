/**
 * 
 * @author https://stackoverflow.com/a/34749873
 */

export function isObject(item) {
  return (item && typeof item === 'object' && !Array.isArray(item));
}


const dedupe = (array) => {
  var a = array.concat();
  for (var i = 0; i < a.length; ++i) {
    for (var j = i + 1; j < a.length; ++j) {
      if (a[i].name === a[j].name)
        a.splice(j--, 1);
    }
  }

  return a;
}

export default function mergeFolders(targetFolder, sourceFolder) {

  let output = Object.assign({}, targetFolder);

  if (isObject(targetFolder) && isObject(sourceFolder)) {
    Object.keys(sourceFolder).forEach(key => {
      if (isObject(sourceFolder[key])) {
        if (!(key in targetFolder))
          Object.assign(output, { [key]: sourceFolder[key] });
        else
          output[key] = mergeFolders(targetFolder[key], sourceFolder[key]);
      } else if (Array.isArray(targetFolder[key]) && Array.isArray(sourceFolder[key])) {
        const result = sourceFolder[key];

        const isStringArray = result.every(s => typeof s === "string") && targetFolder[key].every(f => typeof f === "string");

        if (!isStringArray) {
          targetFolder[key].forEach(file => {
            const existingFile = result.find(f => {
              if (isObject(f)) {
                if ("name" in f) {
                  return f.name === file.name;
                } else if ("program" in f) {
                  return f.program === file.program;
                }
              }
              return f === file;
            });
            if (existingFile) {
              if (isObject(existingFile)) {
                result[result.indexOf(existingFile)] = mergeFolders(file, existingFile);
              } else {
                result[result.indexOf(existingFile)] = file;
              }
            } else {
              result.push(file);
            }
          });
        }

        output[key] = result;
      } else {
        Object.assign(output, { [key]: sourceFolder[key] });
      }
    });
  }
  return output;
}