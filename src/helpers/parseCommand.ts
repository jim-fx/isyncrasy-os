export default (raw) => {
  const [command, ...rawRest] = raw.split(" ");

  const { rest, flags, params } = rawRest.reduce(
    ({ flags, params, rest }, v) => {
      if (v.startsWith("--")) {
        const s = v.replace(/^--/, "").split("=");
        params[s[0]] = s.length > 1 ? parseInt(s[1]) || s[1] : "";
      } else if (v.startsWith("-")) {
        flags.push(v.replace(/^-/, ""));
      } else {
        rest.push(v);
      }

      return { flags, rest, params };
    },
    { flags: [], rest: [], params: {} }
  );

  return {
    command,
    rest,
    raw,
    flags,
    params,
  };
};