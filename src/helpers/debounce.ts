export default function (func, wait) {
  var timeout;

  // die "verzögerte" Funktion
  return function () {

    var context = this, args = arguments;

    // Timeout resetten
    var later = function () {
      timeout = null;
      func.apply(context, args);
    };

    // Timer neu starten
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}