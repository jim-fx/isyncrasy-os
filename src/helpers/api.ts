import type * as SocketIOClient from "socket.io-client";
import io from "socket.io-client";
import ss from "socket.io-stream/socket.io-stream.js";
import { state } from "stores";

const createSocket = (): Promise<SocketIOClient.Socket> =>
  new Promise(async (res, rej) => {
    setTimeout(() => {
      const u = state.get("serverUrl");
      const s = io(u);
      s.on("connect", () => {
        console.log("[API] socket connected to " + u);
        res(s);
        s.off("connect");
      });
    }, 50);
  });

let socket = createSocket();

const request = (
  url,
  {
    method = "GET",
    body = undefined,
    headers = { "Content-Type": "application/json" } as {
      [key: string]: string;
    },
  } = {}
) => {
  const absUrl = state.get("serverUrl") + "/" + url.replace(/^\//, "");
  if (body) {
    return fetch(absUrl, {
      method,
      body,
      headers,
    }).then((res) => res.json());
  }

  return fetch(absUrl, {
    method,
    body,
    headers,
  }).then((res) => res.json());
};

const handleCallback = (func, cb) => {
  if (cb) {
    return func.then((res) => cb(null, res)).catch((err) => cb(err));
  } else {
    return func;
  }
};

export default {
  on: async (event, cb) => {
    const s = await socket;
    if (socket) s.on(event, cb);
    return () => {
      s.off(event);
    };
  },
  onStream: async (event, cb) => {
    const s = ss(await socket);
    if (socket) s.on(event, cb);
    return () => {
      s.off(event);
    };
  },
  emit: async (event, ...args) => {
    const s = await socket;
    if (s.connected) {
      console.log("[API] emit " + event, args);
      s.emit(event, ...args);
    } else {
      console.log("[API] LOST", event, args);
    }
  },
  get: (url, cb?: (err: Error | null, result: any) => void) => {
    console.log(`[API] get ${url}`);
    return handleCallback(request(url), cb);
  },
  post: (url, body, cb?: (err: Error | null, result: any) => void) => {
    console.group(`[API] post ${url}`);
    console.log(body);
    console.groupEnd();
    return handleCallback(
      request(url, { method: "POST", body: JSON.stringify(body) }),
      cb
    );
  },
  upload: (url, body: { [key: string]: any } = {}) => {
    const formdata = new FormData();
    Object.entries(body).forEach(([key, value]) => formdata.append(key, value));
    return request(url, { method: "POST", body: formdata, headers: {} });
  },
};
