import exec from "./exec";
import * as programs from "./index";
export default {
  exec: ({ command, rest }, stdout) => {

    if (command !== "help") return false;

    if (rest.length) {

      rest.map(c => {

        if (c in programs) {
          stdout(programs[c].exec({ command: c, params: { "help": true } }, stdout))
        } else {
          stdout(`program: ${c} not found`);
        }

      })
    } else {
      return stdout([
        "-----------------",
        "Welcome to the ISyncrasyOS helpline.",
        "",
        "[COMMANDS]:",
        " 'startx'",
        "  - starts the visual interface",
        "",
        "-----------------",
      ])
    }
  }
}

