import { state, fs } from "stores";

export default {
  exec: ({ command, rest }, stdout) => {

    if (command !== "cd") return false;

    const newCwd = fs.resolve(state.get("cwd"), rest[0])

    const newDir = fs.get(newCwd);

    if (newDir) {
      state.update(s => {
        s.cwd = newCwd;
        return s;
      })
      return true;
    } else {
      return stdout(["no such file or directory:", newCwd]);
    }
  }
};
