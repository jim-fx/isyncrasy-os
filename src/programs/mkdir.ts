import { state, fs } from "stores";

export default {
  exec: ({ command, rest, flags }, stdout) => {

    if (command !== "mkdir") return;

    const newDir = fs.write(fs.resolve(state.get("cwd"), rest[0]), {
      type: "dir",
      children: []
    }, flags);

    if (newDir) {
      return stdout("created directory: " + rest[0])
    } else {
      return stdout("could not create directory");
    }

  }
}

