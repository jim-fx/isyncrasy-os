
export default {
  exec: ({ command, params }, stdout) => {

    if (command !== "clear" && command !== "cls") return false;

    if ("help" in params) {
      return stdout([
        "Usage: clear",
        "clears the console",
      ])
    }

    return {
      clear: true
    };
  }
};
