import Presentation from "./index.svelte";
import { window } from "stores";

export default {
  id: "ps1",
  app: Presentation,
  exec: function (file) {
    if (file) this.props = { file };
    window.create(this);
    return true;
  },
};
