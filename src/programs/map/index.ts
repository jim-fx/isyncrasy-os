import Map from "./index.svelte";
import { window } from "stores";

export default {
  id: "ma1",
  app: Map,
  icon: "globe",
  exec: function () {
    window.create(this);

    return true;
  },
};
