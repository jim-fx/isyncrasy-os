//@ts-ignore
import { Box, Transform, Mesh, Program, Color } from "ogl";
import { vertex, fragment } from "./shaders";

const WALL_HEIGHT = 3;

const angleOfVector = (x, y) => Math.atan2(x, y);

const distance2D = (x1, y1, x2, y2) => {
  return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
};

const createLine = (gl, program, x1, y1, x2, y2) => {
  const width = 0.2;
  const depth = distance2D(x1, y1, x2, y2);

  const angle = angleOfVector(x2 - x1, y2 - y1);

  const x = x1 + (x2 - x1) * 0.5;
  const z = y1 + (y2 - y1) * 0.5;

  const mesh = new Mesh(gl, {
    geometry: new Box(gl, { width, depth, height: WALL_HEIGHT }),
    program,
  });

  mesh.rotation.y = angle;
  mesh.position.set(x, WALL_HEIGHT / 2, z);

  return mesh;
};

export default (gl, map) => {
  const { width, height, lines } = map.data;

  const scene = new Transform();

  const white = new Program(gl, {
    vertex,
    fragment,
    uniforms: {
      color: {
        value: new Color("#d3d3d3"),
      },
    },
  });

  const ground = new Mesh(gl, {
    geometry: new Box(gl, {
      width: width / 100,
      depth: height / 100,
      height: 0.5,
    }),
    program: white,
  });
  ground.setParent(scene);

  const blue = new Program(gl, {
    vertex,
    fragment,
    uniforms: {
      color: {
        value: new Color("#63d0ff"),
      },
    },
  });

  const red = new Program(gl, {
    vertex,
    fragment,
    uniforms: {
      color: {
        value: new Color("#ff3c2e"),
      },
    },
  });

  lines.forEach((line) => {
    for (let i = 0; i < line.length - 2; i += 2) {
      const x1 = line[i] / 100 - width / 200;
      const y1 = line[i + 1] / 100 - height / 200;
      const x2 = line[i + 2] / 100 - width / 200;
      const y2 = line[i + 3] / 100 - height / 200;

      const corner = new Mesh(gl, {
        geometry: new Box(gl, {
          width: 0.15,
          depth: 0.15,
          height: WALL_HEIGHT + 0.5,
        }),
        program: blue,
      });
      corner.position.set(x1, WALL_HEIGHT / 2, y1);
      corner.setParent(scene);

      const x = x1 + (x2 - x1) * 0.5;
      const z = y1 + (y2 - y1) * 0.5;
      const edgeCenter = new Mesh(gl, {
        geometry: new Box(gl, {
          width: 0.05,
          depth: 0.05,
          height: WALL_HEIGHT,
        }),
        program: red,
      });
      edgeCenter.position.set(x, WALL_HEIGHT, z);
      edgeCenter.setParent(scene);

      createLine(gl, white, x1, y1, x2, y2).setParent(scene);
    }
  });

  return scene;
};
