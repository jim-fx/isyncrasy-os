import Chat from "./index.svelte";
import { create, close } from "stores/window";

export default {
  id: "ch1",
  app: Chat,
  exec: function ({ params = {} } = {}, stdout) {
    create(this);

    if (params && "close" in params) {
      stdout("closing chat");
      close(this.id);
    }

    return true;
  },
};
