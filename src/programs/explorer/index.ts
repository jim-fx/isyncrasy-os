import Explorer from "./index.svelte";
import { window, fs, state } from "stores";

export default {
  id: "ex1",
  app: Explorer,
  exec: function ({ rest }, stdout) {
    if (rest && rest[0]) {
      const cwd = fs.resolve(state.get("cwd"), rest[0]);
      if (cwd) {
        this.props = {
          cwd,
        };
        stdout("opened explorer: " + cwd);
      }
    }

    window.create(this);

    return true;
  },
};
