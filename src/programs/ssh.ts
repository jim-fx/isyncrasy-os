export default {
  exec: ({ command }, stdout) => {

    if (command !== "ssh") return false;

    stdout("eyyy, please say [y]es");

    return {
      stdin: ({ command }) => {

        stdout([command.toLowerCase() === "y" ? "yeeeees" : "noooo"]);

        return true;
      }
    };
  }
}
