import Sound from "./index.svelte";
import { window } from "stores";

export default {
  id: "so1",
  app: Sound,
  exec: function () {
    window.create(this);
    return true;
  },
};
