import Canvas from "./index.svelte";
import { create } from "stores/window";

export default {
  id: "ico1",
  app: Canvas,
  exec: function () {
    create(this);
  },
};
