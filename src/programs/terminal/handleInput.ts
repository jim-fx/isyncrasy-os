import { parseCommand } from "helpers";
import { fs } from "stores";
import * as programs from "..";

const functions = fs
  .get("/usr/bin")
  .children.map((c) => c.name in programs && programs[c.name])
  .filter((s) => !!s);

export default async (line, stdout) => {
  const command = parseCommand(line);

  for (const func of functions) {
    let r = await func.exec(command, stdout);
    if (r) return r;
  }
};
