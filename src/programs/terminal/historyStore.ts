import { writable } from "svelte/store";

let initialHistory = [];

if ("terminalHistory" in localStorage) {
  try {
    initialHistory = JSON.parse(localStorage.getItem("terminalHistory"));
  } catch (error) {}
}

const store = writable(initialHistory);

store.subscribe((v) => {
  localStorage.setItem("terminalHistory", JSON.stringify(v));
});

export default store;
