import Terminal from "./index.svelte";
import { create } from "stores/window";

export default {
  id: "co1",
  app: Terminal,
  exec: function () {
    create(this);
  }
}