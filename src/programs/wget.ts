import { state, fs } from "stores";
import { humanSize } from "helpers";

export default {
  exec: async ({ command, rest }, stdout) => {
    if (command !== "wget") return false;

    let cwd = fs.get(state.get("cwd"));

    let url = rest[0];

    const res = await fetch("https://cors-anywhere.herokuapp.com/" + url, {
      method: "get",
      headers: {
        "x-requested-with": "isyncrasy-os",
      },
    });

    const content = await res.text();

    const cType = res.headers.get("Content-Type");

    cwd.children.push({
      name: "download",
      type: "file",
      content,
    });

    fs.update();

    return stdout([
      `downloaded    ${rest[0]}`,
      `content-type  ${cType}`,
      `size          ${humanSize(content)}`,
    ]);
  },
};
