import { state } from "stores";

export default {
  exec: ({ command, rest }, stdout) => {

    if (command !== "setname") return false;

    const newName = rest[0];

    if (!newName || newName.length > 15) {
      return stdout("that name is too long")
    }

    state.update(s => {
      s.name = newName;
      return s;
    })

    return stdout("set name to " + newName);
  }
}
