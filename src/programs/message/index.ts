import { fs, window } from "stores";
import Mail from "./index.svelte";
import replaceVariables from "./replaceVariables";

const sendMessage = (command, rest, stdout) => {
  const recipient = rest[1];

  if (recipient) {
    stdout(`Sending message from *user* to ${rest[1]}`);

    return {
      stdin: ({ command, rest }) => {
        const content = [command, ...rest];
        stdout(["Wrote message to " + recipient, ...content]);
        return true;
      },
    };
  }

  return stdout("please specify a recipient after the @");
};

const help = (stdout) =>
  stdout([
    "Usage: message [OPTIONS]",
    "- List all messages -",
    "  message",
    "- Read -",
    "  message [number]",
    "- Write -",
    "  message @ [username]",
  ]);

const listAllMessages = (rest, stdout) => {
  const messageDir = fs.get("/home/messages");

  const messages = messageDir.children.filter((c) => c.type === "msg");

  const newMessages = messages.filter((msg) => !msg.read);

  if (!rest.length) {
    return stdout([
      `You have ${newMessages.length} new messages`,
      ...messages.map(
        (msg, i) =>
          `{${i}} ${msg.read ? "READ" : "UNREAD"} from [${msg.from}] to [${
            msg.to
          }]`
      ),
      "To read a message please type 'message [number]'",
    ]);
  }

  const numbers = rest.map(parseInt).filter((d) => typeof d === "number");

  const output = numbers
    .map((num) => {
      const msg = messages[num];

      if (!msg) return [`message [${num}] not found`];
      msg.read = true;

      return [
        `{${num}} from [${msg.from}] to [${msg.to}]`,
        "---------------------",
        ...replaceVariables(msg.content),
        "---------------------",
      ];
    })
    .flat();

  fs.update();

  return stdout(output);
};

export default {
  id: "mail1",
  app: Mail,
  icon: "mail",
  exec: function (input, stdout) {
    if (!input || JSON.stringify(input) === "{}") return window.create(this);

    const { command, rest, params } = input;

    if (command !== "message" && command !== "messages") return false;

    if (params && "help" in params) return help(stdout);

    if (rest[0] === "@") return sendMessage(command, rest, stdout);
    const messageDir = fs.get("/home/messages");

    if (!messageDir && messageDir.children && messageDir.children.length) {
      return stdout(`You have 0 new messages`);
    }

    return listAllMessages(rest, stdout);
  },
};
