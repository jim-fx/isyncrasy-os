import { state } from "stores";

const replaceVars = string => string.replace(/(?<=\{).*?(?=\})/, (variable) => state.get(variable)).replace("{", "").replace("}", "");

export default (lines) => {
  return Array.isArray(lines) ? lines.map(replaceVars) : replaceVars(lines);
}
