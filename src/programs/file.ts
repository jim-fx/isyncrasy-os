import { state, fs } from "stores";

export default {
  exec: ({ command, rest }, stdout) => {

    if (command !== "file") return false;

    const file = fs.get(fs.resolve(state.get("cwd"), rest[0]));

    if (file) {
      return stdout(file.name + ": " + file.type);
    } else {
      return stdout(["no such file or directory:", rest[0]])
    }
  }
}

