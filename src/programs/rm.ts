import { state, fs } from "stores";

export default {
  exec: ({ command, rest, flags }, stdout) => {

    if (command !== "rm") return false;

    const output = rest.map(s =>
      fs.remove(fs.resolve(state.get("cwd"), s), flags) ? "removed " + s : "could not remove " + s
    )

    return stdout(output);
  }
}
