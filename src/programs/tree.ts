import { fs, state } from "stores";

const traverse = (folder, params, flags, depth = 0, og = []) => {
  if (params["max-depth"] && depth >= params["max-depth"]) return;

  og.push(
    "-".repeat(depth * 2 + 1) +
      (flags.includes("l") ? folder.type + ": " : " ") +
      folder.name
  );

  if (folder.type === "dir" && folder.children.length) {
    folder.children.forEach((c) => traverse(c, params, flags, depth + 1, og));
  }

  return og;
};

export default {
  exec: ({ command, rest, params, flags }, stdout) => {
    if (command !== "tree") return false;

    let cwd = fs.get(state.get("cwd"));
    if (rest[0]) cwd = fs.get(fs.resolve(state.get("cwd"), rest[0]));

    const output = cwd
      ? traverse(cwd, params, flags)
      : "could not find directory";

    return stdout(output);
  },
};
