// cli apps
export { default as cd } from "./cd";
export { default as exec } from "./exec";
export { default as file } from "./file";
export { default as ls } from "./ls";
export { default as mkdir } from "./mkdir";
export { default as pwd } from "./pwd";
export { default as rm } from "./rm";
export { default as touch } from "./touch";
export { default as tree } from "./tree";
export { default as clear } from "./clear";
export { default as startx } from "./startx";
export { default as echo } from "./echo";
export { default as cat } from "./cat";
export { default as wget } from "./wget";
export { default as ssh } from "./ssh";
export { default as setname } from "./setname";
export { default as help } from "./help";
export { default as whoami } from "./whoami";
export { default as cowsay } from "./cowsay";

// hybrid apps
export { default as message } from "./message";

// gui apps
export { default as terminal } from "./terminal";
export { default as manual } from "./manual";
export { default as iconviewer } from "./iconviewer";
export { default as sound } from "./sound";
export { default as volume } from "./volume";
export { default as dimension16 } from "./dimension16";
export { default as explorer } from "./explorer";
export { default as viewer3D } from "./viewer3D";
export { default as chat } from "./chat/index";
export { default as presentation } from "./presentation";
export { default as map } from "./map";
export { default as airship } from "./airship";
