import { state, fs } from "stores";

export default {
  exec: ({ command, rest, params }, stdout) => {

    if (command !== "cat") return false;

    if ("help" in params) {
      return stdout([
        "Usage: cat [OPTION]... [FILE]...",
        "Concatenate FILE(s) to standard output",
        "",
        "Examples:",
        "   cat f g  Output f's contents, then standard input, then g's contents",
      ]);
    }

    const elements = rest.map(path => fs.get(fs.resolve(state.get("cwd"), path)));

    const output = elements.map(el => {

      if (el.type === "dir") {
        return `cat: ${el.name}: Is a directory`;
      } else if (el.content) {
        return [`${el.name}:`, ...(Array.isArray(el.content) ? el.content : el.content.includes("\n") ? el.content.split("\n") : el.content)];
      } else if (el.exec) {
        return [`${el.name}:`, ...el.exec.toString().split("\n")];
      } else {
        return JSON.stringify(el, null, 2).split("\n");
      }

    }).flat();

    return stdout(output);
  }
}
