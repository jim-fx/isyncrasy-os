import { state } from "stores";


export default {
  exec: ({ command }, stdout) => {

    if (command !== "pwd") return false;

    return stdout(state.get("cwd"));
  }
}