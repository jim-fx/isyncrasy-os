export default {
  exec: ({ command, rest: output }, stdout) => {
    if (command !== "echo") return false;
    return stdout(output);
  }
}