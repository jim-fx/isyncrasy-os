import { fs, state } from "stores";

export default {
  exec: ({ command, rest, flags }, stdout) => {

    if (command !== "touch") return;

    const newDir = fs.write(fs.resolve(state.get("cwd"), rest[0]), {
      type: "file",
      children: []
    }, flags);

    if (newDir) {
      return stdout("created file: " + rest[0])
    } else {
      return stdout("could not create file");
    }

  }
}
