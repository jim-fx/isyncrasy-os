import state from "stores/state";

export default {
  exec: ({ command, params }) => {
    if (command !== "startx") return;

    state.update(s => {
      s.showBoot = "boot" in params ? !s.showBoot : s.showBoot;
      s.screen = s.screen === "boot" ? "desktop" : "boot";
      return s;
    })

    return true;
  }
}