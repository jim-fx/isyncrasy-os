import { state } from "stores";

export default {
  exec: ({ command }, stdout) => {

    if (command !== "whoami") return;

    return stdout(state.get("name"));

  }
}
