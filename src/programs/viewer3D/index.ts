import viewer3D from "./index.svelte";
import { create } from "stores/window";

export default {
  id: "3d1",
  app: viewer3D,
  props: {
    active: "Suzanne",
  },
  exec: function () {
    create(this);
  }
}