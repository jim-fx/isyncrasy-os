import { parseCommand } from "helpers";
import { fs, state } from "stores";
import * as programs from "./index";

export default {
  exec: ({ command, rest, raw }, stdout) => {
    if (!command.startsWith("./") && command !== "exec") return;

    let path = command.startsWith("./") ? command : rest.splice(0, 1)[0];

    let executable = fs.get(fs.resolve(state.get("cwd"), path));

    if (!executable) {
      //try to find the file by program
      const currentDir = fs.get(state.get("cwd"));
      executable = currentDir.children.find((c) => path.includes(c.program));
    }

    if (executable) {
      if (executable.type === "exec" && executable.name in programs) {
        return programs[executable.name].exec(
          parseCommand(raw.replace(/^.\//, "")),
          stdout
        );
      } else if (executable.type === "exec" && executable.program in programs) {
        return programs[executable.program].exec(
          parseCommand(raw.replace(/^.\//, "")),
          stdout
        );
      } else if (executable.type in programs) {
        console.log(programs[executable.type]);
      } else {
        return stdout("cant call type: " + executable.type);
      }
    } else {
      return stdout(["no such file or directory:", command]);
    }
  },
};
