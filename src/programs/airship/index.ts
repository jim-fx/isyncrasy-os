import App from "./index.svelte";
import { create, close } from "stores/window";

export default {
  id: "air1",
  app: App,
  exec: function () {
    create(this);
    return true;
  },
};
