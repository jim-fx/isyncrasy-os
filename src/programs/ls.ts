import { fs, state } from "stores";

export default {
  exec: ({ command, rest = [], flags = [] }, stdout) => {
    if (command !== "ls" && command !== "dir") return false;

    let cwd = fs.get(state.get("cwd"));
    if (rest[0]) cwd = fs.get(fs.resolve(state.get("cwd"), rest[0]));

    const extraInfo = flags.includes("l");

    if (cwd.children.length) {
      const output = cwd.children.map(
        (c) => (extraInfo ? c.type + ": " : "") + (c.name || c.program)
      );
      stdout(output);
    } else {
      stdout("directory empty");
    }

    return true;
  },
};
