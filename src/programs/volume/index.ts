import App from "./index.svelte";
import { create } from "stores/window";

export default {
  id: "calc1",
  app: App,
  exec: function () {
    create(this);
  }
}