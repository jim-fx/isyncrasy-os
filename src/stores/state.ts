import { genName } from "helpers";
import { get, writable, Writable } from "svelte/store";
import * as fs from "./fs";

interface Config {
  pass: string;
  activeMap: any;
  cwd: string;
  name: string;
  screen: string;
  showBoot: boolean;
  serverUrl: string;
}

const setDefaults = (config: Config) => {
  const {
    cwd = "/",
    name = genName(),
    screen = "boot",
    showBoot = true,
    serverUrl = window.location.href.includes("isyncrasy")
      ? "https://api.isyncrasy.com"
      : "__API_URL__",
  } = config;

  return {
    ...config,
    cwd,
    showBoot,
    name,
    screen,
    serverUrl,
  };
};

const { data } = fs.get("/usr/config");

interface W<T> extends Writable<T> {
  get?: (key?: string) => any;
}

const stateStore: W<Config> = writable(setDefaults(data || {}));

stateStore.get = (key: string) => {
  if (!key) return;
  const s = get(stateStore);
  return key ? s[key] : s;
};

stateStore.subscribe((s) => {
  fs.write("/usr", { name: "config", type: "config", data: s }, { o: true });
});

export default stateStore;
