import { writable } from "svelte/store";
import * as p from "../programs";
const programs: Program[] = Object.values(p);

interface Program {
  id?: string;
  app?: any;
  minimized?: boolean;
  title?: string;
  x?: number;
  y?: number;
  width?: number;
  height?: number;
  exec: (options?: any, stdout?: any) => any;
}

let windows: { [key: string]: Program } = {};
if ("windows" in localStorage)
  windows = JSON.parse(localStorage.getItem("windows"));

Object.entries(windows).forEach(([key, w]) => {
  if (!w.app) {
    const prog = programs.find((p) => p.id === w.id);
    w.app = prog.app;
  }
});

let curI = 0;
let ids = [];
if ("ids" in localStorage) ids = JSON.parse(localStorage.getItem("ids"));

export const store = writable(windows);

setTimeout(() => {
  store.subscribe((_s) => {
    windows = _s;
    localStorage.setItem("windows", JSON.stringify(_s));
  });
}, 10);

export const minimize = (id) => {
  if (id in windows) windows[id].minimized = true;
  store.set(windows);
};

export const setTitle = (id, title) => {
  if (id in windows) windows[id].title = title;
};

export const maximize = (id) => {
  if (id in windows) windows[id].minimized = false;
  store.set(windows);
};

export const close = (id) => {
  delete windows[id];
  store.set(windows);
};

let activeWindow;
export const setActive = (id) => {
  const newWindow = id in windows && windows[id];
  if (newWindow) {
    if (activeWindow) activeWindow.active = false;
    activeWindow = newWindow;
    activeWindow.minimized = false;
    activeWindow.active = true;
    store.set(windows);
  }
};

export const setTransform = (id, x, y, width, height) => {
  if (id in windows) {
    windows[id].x = x;
    windows[id].y = y;
    windows[id].width = width;
    windows[id].height = height;
  }
  store.set(windows);
};

const assureID = (w) => {
  if (!w.id) {
    if (!ids[curI]) ids[curI] = "win" + curI;

    w.id = ids[curI];

    curI++;
  }
};

export const create = (w) => {
  assureID(w);

  console.groupCollapsed("[WINDOW] create new window");
  console.log(w);
  console.groupEnd();

  if (w.id in windows) {
    windows[w.id] = Object.assign(windows[w.id], w);
  } else {
    windows[w.id] = w;
  }

  setActive(w.id);

  store.set(windows);
};
