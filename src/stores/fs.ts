import { writable } from "svelte/store";
//@ts-ignore
import initialFS from "./initialFS.json";
import { mergeFS } from "helpers";

const fsID = "fs_main";
//Read saved state from localStorage
let fs;

try {
  fs =
    fsID in localStorage
      ? mergeFS(initialFS, JSON.parse(localStorage.getItem(fsID)))
      : initialFS;
  console.log("[FS] loaded fs " + fsID + " from localStorage");
} catch (error) {
  console.log("[FS] could not load fs " + fsID + " from localStorage");
  console.error(error);
}

export const store = writable(fs);

setTimeout(() => {
  store.subscribe((s) => {
    if (s) {
      fs = s;
      localStorage.setItem(fsID, JSON.stringify(fs));
      console.log("[FS] saved to localStorage");
    }
  });
}, 100);

export const resolve = (abs, command) => {
  let path = abs.split("/").filter((d) => !!d);
  const commands = command.split("/").filter((d) => !!d);

  if (!commands.length) return "/";

  commands.forEach((c) => {
    switch (c) {
      case "..":
        path.pop();
        break;
      case ".":
        break;
      default:
        path.push(c);
    }
  });

  return "/" + path.join("/");
};

export const get = (path) => {
  console.groupCollapsed("[FS] get " + path);
  if (!path) return false;

  const dirs = path.split("/").filter((d) => !!d);

  let curFile = fs;
  for (let i = 0; i < dirs.length; i++) {
    const d = dirs[i];
    let nextFile = curFile.children.find((file) => file.name === d);
    if (nextFile) curFile = nextFile;
    else {
      console.log("File not found");
      console.groupEnd();
      return false;
    }
  }

  console.log(curFile);
  console.groupEnd();

  return curFile;
};

export const remove = (path, flags = []) => {
  const dirs = path.split("/").filter((d) => !!d);

  const removeDirs = flags.includes("r");

  const finalDir = dirs.pop();

  let curDir = fs;
  for (let i = 0; i < dirs.length; i++) {
    const d = dirs[i];
    let nextDir = curDir.children.find((folder) => folder.name === d);
    if (nextDir) curDir = nextDir;
    else return false;
  }

  if (!curDir.children) return false;

  const foundDir = curDir.children.find((d) => d.name === finalDir);

  if (
    !foundDir ||
    (foundDir.type === "dir" && !removeDirs) ||
    foundDir.protected
  )
    return false;

  curDir.children = curDir.children.filter((c) => c.name !== finalDir);

  store.set(fs);

  return true;
};

export const write = (path, el, flags = {}) => {
  const dirs = path.split("/").filter((d) => !!d);

  const createParentDirs = !!flags["p"];
  const overwriteFile = !!flags["o"];

  const fileName = el.name ? el.name : dirs.pop();
  el.name = fileName;

  console.groupCollapsed("[FS] write file " + path + "/" + fileName);
  console.log(el);
  console.groupEnd();

  let curDir = fs;
  for (let i = 0; i < dirs.length; i++) {
    const d = dirs[i];
    let nextDir = curDir.children.find((folder) => folder.name === d);
    if (nextDir) curDir = nextDir;
    else {
      if (!createParentDirs) return false;
      nextDir = {
        name: d,
        type: "dir",
        children: [],
      };
      curDir.children.push(nextDir);
      curDir = nextDir;
    }
  }

  if (curDir.type === "dir") {
    if (!curDir.children) curDir.children = [];

    const foundFile = curDir.children.find((d) => d.name === fileName);

    if (foundFile) {
      if (!overwriteFile) return false;
      curDir.children[curDir.children.indexOf(foundFile)] = el;
    } else {
      curDir.children.push(el);
    }

    store.set(fs);

    return el;
  }

  return false;
};

export const update = () => store.set(fs);
